/*
 * adxl345.c
 *
 *  Created on: May 7, 2012
 *      Author: renan
 */
#include "adxl345.h"

//#define XL345_FORMAT	XL345_FULL_RESOLUTION | XL345_DATA_JUST_RIGHT | XL345_RANGE_16G
//#define TAP_DUR		0x08		// 625 us/LSB, 0x08=5ms
//#define THRESH_TAP	0x10		// 62.5mg/LSB, 0x10=1.0g
//#define TAP_AXES	(XL345_TAP_X_ENABLE+XL345_TAP_Y_ENABLE+XL345_TAP_Z_ENABLE)
//const unsigned char adxl345_init_data[] =
//{
//	XL345_THRESH_TAP, THRESH_TAP,		// 0x1d: 62.5mg/LSB, 0x08=0.5g
//	XL345_OFSX, 0x00,					// 0x1e: 0xff
//	XL345_OFSY, 0x00,					// 0x1f: 0x05
//	XL345_OFSZ, 0x00,					// 0x20: 0xff
//	XL345_DUR, TAP_DUR,					// 0x21: 625 us/LSB, 0x10=10ms
//	XL345_LATENT, 0x00,					// 0x22: no double tap
//	XL345_WINDOW, 0x00,					// 0x23: no double tap
//	XL345_THRESH_ACT, 0x20,				// 0x24: 62.5mg/LSB, 0x20=2g
//	XL345_THRESH_INACT, 0x03,			// 0x25: 62.5mg/LSB, 0x03=0.1875g
//	XL345_TIME_INACT, 0x02,				// 0x26: 1s/LSB, 0x02=2s
//	XL345_ACT_INACT_CTL, 0x00,			// 0x27: no activity interrupt
//	XL345_THRESH_FF, 0x00,				// 0x28: no free fall
//	XL345_TIME_FF, 0x00,				// 0x29: no free fall
//	XL345_TAP_AXES, TAP_AXES,			// 0x2a: TAP_Z enable
//	XL345_BW_RATE, XL345_RATE_100,		// 0x2cadxl345
//	XL345_POWER_CTL, XL345_STANDBY,		// 0x2d: standby while changing int
//	XL345_INT_ENABLE, 0x00			,	// 0x2e: disable
//	XL345_INT_MAP, 0x00,				// 0x2f: all interrupts to INT1
//	XL345_DATA_FORMAT, XL345_FORMAT,	// 0x31
//	XL345_FIFO_CTL, 0x00,				// 0x38
//
//	XL345_INT_ENABLE, XL345_SINGLETAP,	// 0x2e: single tap enable
//	XL345_POWER_CTL, XL345_MEASURE,		// 0x2d: measure
//	0x00								// end of configuration
//};

//uint32_t adxl345_timer;
//
//void adxl345_timering ()
//{
//	if (adxl345_timer > 0)
//		adxl345_timer--;
//}

int adxl345_init_alt ()
{
//	unsigned char* data = (unsigned char*)adxl345_init_data;
//	unsigned char reg;
////	int error;
////
////	// error setup
////	if (error = setjmp(i2c_context))
////	{
//////		init_USCI_B0(LCD_ADDRESS);
////		return error;
////	}
//
//	// check DEVID
////	ADXL345_read(XL345_DEVID, &reg, 1);
//	reg = XL345_DEVID;					// ADXL345 register
//	i2c0_write_one_byte(XL345_ALT_ADDR, reg, 0);
//	unsigned char devId;
//	i2c0_read_only_byte(XL345_ALT_ADDR, reg, &devId);
//	char msg[50];
//	memset (msg, 0x00, sizeof (msg));
//	sprintf (msg, "Device ID = %X\r\n", devId);
//	UART0_PrintString (msg);
//
////	if (reg != XL345_ID) ERROR2(SYS_ERR_XL345ID);
//
//	// initialize adxl345
//	while (*data)
//	{
//		reg = *data++;					// get register
//		i2c0_write_one_byte(XL345_ALT_ADDR, reg, *data++);	// write data
//	}
//	return 0;							// success

//	adxl345_timer = 0;
	i2c0_write_one_byte(XL345_ALT_ADDR, XL345_DATA_FORMAT, 0x0b);
	i2c0_write_one_byte(XL345_ALT_ADDR, XL345_POWER_CTL, 0x08);
	i2c0_write_one_byte(XL345_ALT_ADDR, XL345_INT_ENABLE, 0x80);

	i2c0_write_one_byte(XL345_ALT_ADDR, XL345_FIFO_CTL, XL345_FIFO_MODE_STREAM);

//	adxl345_timer = 2;
//	while (adxl345_timer != 0);
	msDelay (2);

//	unsigned char data;
//	unsigned char pc;
//	unsigned char ie;
//	i2c0_read_one_byte (XL345_ALT_ADDR, XL345_DATA_FORMAT, &data);
//	i2c0_read_one_byte (XL345_ALT_ADDR, XL345_POWER_CTL, &pc);
//	i2c0_read_one_byte (XL345_ALT_ADDR, XL345_INT_ENABLE, &ie);
//
//	char msg[50];
//	while (1)
//	{
////		memset (msg, 0x00, sizeof (msg));
////		sprintf (msg, "XL345_DATA_FORMAT = %X\r\n", data);
////		UART0_PrintString (msg);
////		memset (msg, 0x00, sizeof (msg));
////		sprintf (msg, "XL345_POWER_CTL = %X\r\n", pc);
////		UART0_PrintString (msg);
////		memset (msg, 0x00, sizeof (msg));
////		sprintf (msg, "XL345_INT_ENABLE = %X\r\n", ie);
////		UART0_PrintString (msg);
////
////		UART0_PrintString ("Leitura de varios bytes\r\n\r\n");
//
//		uint8_t varios[64];
//		memset (&varios, 0x00, 64);
//		i2c0_read_bytes (XL345_ALT_ADDR, XL345_DATAX0, 5, &varios[0]);
//
//		memset (msg, 0x00, sizeof (msg));
//
//		int loop;
//		for (loop = 0; loop < 6; loop++)
//		{
//			memset (msg, 0x00, sizeof (msg));
//			sprintf (msg, "%X ", varios[loop]);
//			UART0_PrintString (msg);
//		}
//		UART0_PrintString ("\r");
//		adxl345_timer = 1000;
//		while (adxl345_timer  != 0);
//	}
	return 0;
}

int adxl345_init ()
{
	adxl345_init_alt();
	return 0;
	unsigned char devId;
	adxl345_device_id (&devId);

//	char msg[50];
//	memset (msg, 0x00, sizeof (msg));
//	sprintf (msg, "Device ID = %X\r\n", devId);
//	UART0_PrintString (msg);

	// Clear some registers
	i2c0_write_bytes (XL345_ALT_WRITE, XL345_INT_ENABLE, 1, 0x00);
	i2c0_write_bytes (XL345_ALT_WRITE, XL345_POWER_CTL, 1, 0x00);


//	adxl345_write_byte (XL345_DATA_FORMAT, XL345_RANGE_2G | XL345_FULL_RESOLUTION | XL345_DATA_JUST_RIGHT | XL345_INT_HIGH, 0);
//	adxl345_write_byte (XL345_DATA_FORMAT, 0x0b, 0);
//	adxl345_write_byte (XL345_BW_RATE, XL345_RATE_25 | XL345_LOW_NOISE, 0);
//	adxl345_write_byte (XL345_BW_RATE, XL345_RATE_50, 0);

	i2c0_write_one_byte (XL345_ALT_WRITE, XL345_DATA_FORMAT, XL345_RANGE_2G | XL345_FULL_RESOLUTION | XL345_DATA_JUST_RIGHT | XL345_INT_HIGH);
//	i2c0_write_one_byte (XL345_ALT_WRITE, XL345_DATA_FORMAT, XL345_RANGE_2G | XL345_FULL_RESOLUTION | XL345_DATA_JUST_RIGHT | XL345_INT_LOW);
	i2c0_write_one_byte (XL345_ALT_WRITE, XL345_BW_RATE, XL345_RATE_25 | XL345_LOW_NOISE);

	  // map all interrupts to INT1 (set bits to 0) and enable DATA_READY
	i2c0_write_bytes (XL345_ALT_WRITE, XL345_INT_MAP, 1, 0x00);
	i2c0_write_one_byte (XL345_ALT_WRITE, XL345_INT_ENABLE, XL345_DATAREADY);

	  // read data then status to clear Data Ready and Overrun
	short x;
	short y;
	short z;
	adxl345_xyz (&x, &y, &z);
	uint8_t b = 0;

	  //rc|=i2cGetReg(accAddr, XL345_INT_SOURCE, &b);
//	adxl345_write_byte (XL345_INT_SOURCE, 0x00, 1);
//	adxl345_read_byte (1);
//	b = I2CSlaveBuffer[0];

	i2c0_write_bytes (XL345_ALT_WRITE, XL345_INT_SOURCE, 1, 0x00);
	i2c0_read_bytes (XL345_ALT_ADDR, XL345_INT_SOURCE, 1, &b);

	  // start measuring
//	adxl345_write_byte (XL345_POWER_CTL, XL345_MEASURE, 0);

	//adxl345_fifo_mode (XL345_FIFO_MODE_FIFO);
//	adxl345_write_byte (XL345_POWER_CTL, XL345_MEASURE, 0);
//	adxl345_write_byte (XL345_INT_ENABLE, XL345_DATAREADY, 0);

	i2c0_write_one_byte (XL345_ALT_WRITE, XL345_POWER_CTL, XL345_MEASURE);
	return 0;
}

int adxl345_device_id (unsigned char *dev_id)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_DEVID, dev_id);
}

int adxl345_fifo_mode_w (unsigned char mode)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_FIFO_CTL, mode);
}

int adxl345_xyz (volatile short *x, volatile short *y, volatile short *z)
{
	int ret = FALSE;

	uint8_t status = 0;
	i2c0_read_one_byte (XL345_ALT_ADDR, XL345_INT_SOURCE, &status);

	  // always read the data registers to clear overrun, etc.
	uint8_t data[7];
	memset (&data, 0x00, 7);
	ret = i2c0_read_bytes (XL345_ALT_ADDR, XL345_DATAX0, 6, data);

	  // The time from INT1 raised until all 6 bytes are read is 0.332ms,
	  // when using i2c at nominally 400 kHz (actual bus clock = 375khz
	  // on LPC1768 with 96Mhz clock set)

	if (!(status & XL345_DATAREADY))
		ret = FALSE;
	else
	{
		*x = (data[1] << 8) | data[0];
		*y = (data[3] << 8) | data[2];
		*z = (data[5] << 8) | data[4];
		ret = TRUE;
	}

	return ret;
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
int adxl345_getDeviceID (unsigned char *dev_id)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_DEVID, dev_id);
}

int adxl345_getTapThreshold (unsigned char *threshold)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_THRESH_TAP, threshold);
}

int adxl345_setTapThreshold (unsigned char threshold)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_THRESH_TAP, threshold);
}

int adxl345_getOffset (unsigned char *x, unsigned char *y, unsigned char *z)
{
	int ret = FALSE;
	unsigned char data[7];
	memset (&data, 0x00, 7);
	ret = i2c0_read_bytes (XL345_ALT_ADDR, XL345_OFSX, 6, data);
	*x = (data[1] << 8) | data[0];
	*y = (data[3] << 8) | data[2];
	*z = (data[5] << 8) | data[4];
	return ret;
}

int adxl345_setOffset (unsigned char *ox, unsigned char *oy, unsigned char *oz)
{
	int ret = FALSE;
	unsigned char data[4];
	memset (&data, 0x00, 7);
	data[0] = *ox;
	data[1] = *oy;
	data[2] = *oz;
	ret = i2c0_write_bytes (XL345_ALT_ADDR, XL345_OFSX, 3, data);
	return ret;
}

int adxl345_getTapDuration (float *duration)
{
	unsigned char *dur = 0;
	if (i2c0_read_one_byte (XL345_ALT_ADDR, XL345_DUR, dur))
	{
		*duration = *dur * 625;
		return TRUE;
	}
	else
		return FALSE;
}
int adxl345_setTapDuration (float duration_us)
{
    unsigned char dur = (unsigned char)((duration_us * 255) / 625);
    return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_DUR, dur);
}

int adxl345_getTapLatency (float *latency)
{
	unsigned char *lat = 0;
	if (i2c0_read_one_byte (XL345_ALT_ADDR, XL345_LATENT, lat))
	{
		*latency = *lat * 1.25;
		return TRUE;
	}
	else
		return FALSE;
}

int adxl345_setTapLatency (float latency)
{
    unsigned char lat = (unsigned char)((latency * 255) / 1.25);
    return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_LATENT, lat);
}

int adxl345_getWindowTime (float *time)
{
	unsigned char *t = 0;
	if (i2c0_read_one_byte (XL345_ALT_ADDR, XL345_WINDOW, t))
	{
		*time = *t * 1.25;
		return TRUE;
	}
	else
		return FALSE;
}

int adxl345_setWindowTime (float time)
{
    unsigned char t = (unsigned char)((time * 255) / 1.25);
    return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_WINDOW, t);
}

int adxl345_getActivityThreshold (float *thresh)
{
	unsigned char *t = 0;
	if (i2c0_read_one_byte (XL345_ALT_ADDR, XL345_THRESH_ACT, t))
	{
		*thresh = *t * 62.5;
		return TRUE;
	}
	else
		return FALSE;
}

int adxl345_setActivityThreshold (float thresh)
{
    unsigned char t = (unsigned char)((thresh * 255) / 62.5);
    return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_THRESH_ACT, t);
}

int adxl345_getInactivityThreshold (float *thresh)
{
	unsigned char *t = 0;
	if (i2c0_read_one_byte (XL345_ALT_ADDR, XL345_THRESH_INACT, t))
	{
		*thresh = *t * 62.5;
		return TRUE;
	}
	else
		return FALSE;
}

int adxl345_setInactivityThreshold (float thresh)
{
    unsigned char t = (unsigned char)((thresh * 255) / 62.5);
    return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_THRESH_INACT, t);
}

int adxl345_getTimeInactivity (unsigned char *time)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_TIME_INACT, time);
}

int adxl345_setTimeInactivity (unsigned char time)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_TIME_INACT, time);
}

int adxl345_getActivityInactivityControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_ACT_INACT_CTL, settings);
}

int adxl345_setActivityInactivityControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_ACT_INACT_CTL, settings);
}

int adxl345_getFreeFallThreshold (float *thresh)
{
	unsigned char *t = 0;
	if (i2c0_read_one_byte (XL345_ALT_ADDR, XL345_THRESH_FF, t))
	{
		*thresh = *t * 62.5;
		return TRUE;
	}
	else
		return FALSE;
}

int adxl345_setFreeFallThreshold (float thresh)
{
	unsigned char t = (unsigned char)((thresh * 255) / 62.5);
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_THRESH_FF, t);
}

int adxl345_getFreeFallTime (unsigned char *freefall)
{
	unsigned char *f = 0;
	int ret = i2c0_read_one_byte (XL345_ALT_ADDR, XL345_TIME_FF, f);
	*freefall = *f / 5;
	return ret;
}

int adxl345_setFreeFallTime (unsigned char freefall)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_TIME_FF, freefall * 5);
}

int adxl345_getTapAxisControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_TAP_AXES, settings);
}

int adxl345_setTapAxisControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_TAP_AXES, settings);
}

int adxl345_getTapSource (unsigned char *tapSource)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_ACT_TAP_STATUS, tapSource);
}

int adxl345_setDataRateAndPowerModeControl (unsigned char mode)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_BW_RATE, mode);
}

int adxl345_getDataRateAndPowerModeControl (unsigned char *mode)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_BW_RATE, mode);
}

int adxl345_setPowerSavingControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_POWER_CTL, settings);
}

int adxl345_getPowerSavingControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_POWER_CTL, settings);
}

int adxl345_setInterruptEnableControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_INT_ENABLE, settings);
}

int adxl345_getInterruptEnableControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_INT_ENABLE, settings);
}

int adxl345_setInterruptMappingControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_INT_MAP, settings);
}

int adxl345_getInterruptMappingControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_INT_MAP, settings);
}

int adxl345_getInterruptSource (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_INT_SOURCE, settings);
}

int adxl345_setDataFormatControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_DATA_FORMAT, settings);
}

int adxl345_getDataFormatControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_DATA_FORMAT, settings);
}

int adxl345_getOutput (volatile short *x, volatile short *y, volatile short *z)
{
	int ret = FALSE;

	uint8_t status = 0;
	i2c0_read_one_byte (XL345_ALT_ADDR, XL345_INT_SOURCE, &status);

	  // always read the data registers to clear overrun, etc.
	uint8_t data[7];
	memset (&data, 0x00, 7);
	ret = i2c0_read_bytes (XL345_ALT_ADDR, XL345_DATAX0, 6, data);

	// The time from INT1 raised until all 6 bytes are read is 0.332ms,
	// when using i2c at nominally 400 kHz (actual bus clock = 375khz
	// on LPC1768 with 96Mhz clock set)

	if (!(status & XL345_DATAREADY))
		ret = FALSE;
	else
	{
		*x = (data[1] << 8) | data[0];
		*y = (data[3] << 8) | data[2];
		*z = (data[5] << 8) | data[4];
		ret = TRUE;
	}
	return ret;
}

int adxl345_getFifoControl (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_FIFO_CTL, settings);
}

int adxl345_setFifoControl (unsigned char settings)
{
	return i2c0_write_one_byte (XL345_ALT_ADDR, XL345_FIFO_CTL, settings);
}

int adxl345_getFifoStatus (unsigned char *settings)
{
	return i2c0_read_one_byte (XL345_ALT_ADDR, XL345_FIFO_STATUS, settings);
}
