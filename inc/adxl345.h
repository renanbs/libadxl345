/*
 * adxl345.h
 *
 *  Created on: May 7, 2012
 *      Author: renan
 */

#ifndef ADXL345_H_
#define ADXL345_H_

#include "LPC17xx.h"
#include "types.h"
#include "i2c.h"
#include "sysTick.h"
#include "XL345.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//extern volatile uint8_t I2CMasterBuffer[BUFSIZE];
//extern volatile uint8_t I2CSlaveBuffer[BUFSIZE];
//extern volatile uint32_t I2CReadLength;
//extern volatile uint32_t I2CWriteLength;

//void adxl345_timering ();
//int adxl345_write_byte (uint8_t reg, uint8_t data, uint8_t read_len);
//int adxl345_read_byte (uint8_t len);
int adxl345_init ();
int adxl345_init_alt ();
int adxl345_fifo_mode_w (unsigned char mode);
int adxl345_device_id (unsigned char *dev_id);
int adxl345_xyz (volatile short *x, volatile short *y, volatile short *z);//(short *x, short *y, short *z);


/**
 * Read the device ID register on the device.
 *
 * @return The device ID code [0xE5]
 */
int adxl345_getDeviceID (unsigned char *dev_id);
/**
 * Read the tap threshold on the device.
 *
 * @return The tap threshold as an 8-bit number with a scale factor of
 *         62.5mg/LSB.
 */
int adxl345_getTapThreshold (unsigned char *threshold);
/**
 * Set the tap threshold.
 *
 * @param The tap threshold as an 8-bit number with a scale factor of
 *        62.5mg/LSB.
 */
int adxl345_setTapThreshold (unsigned char threshold);
/**
* Get the current offset for a particular axis.
*
* @param axis 0x00 -> X-axis
*             0x01 -> Y-axis
*             0x02 -> Z-axis
* @return The current offset as an 8-bit 2's complement number with scale
*         factor 15.6mg/LSB.
*/
int adxl345_getOffset (unsigned char *x, unsigned char *y, unsigned char *z);
/**
* Set the offset for a particular axis.
*
* @param axis 0x00 -> X-axis
*             0x01 -> Y-axis
*             0x02 -> Z-axis
* @param offset The offset as an 8-bit 2's complement number with scale
*               factor 15.6mg/LSB.
*/
int adxl345_setOffset (unsigned char *ox, unsigned char *oy, unsigned char *oz);
/**
 * Get the tap duration required to trigger an event.
 *
 * @return The max time that an event must be above the tap threshold to
 *         qualify as a tap event, in microseconds.
 */
//float adxl345_getTapDuration(void);
int adxl345_getTapDuration (float *duration);
/**
 * Set the tap duration required to trigger an event.
 *
 * @param duration_us The max time that an event must be above the tap
 *                    threshold to qualify as a tap event, in microseconds.
 *                    Time will be normalized by the scale factor which is
 *                    625us/LSB. A value of 0 disables the single/double
 *                    tap functions.
 */
int adxl345_setTapDuration (float duration_us);
/**
 * Get the tap latency between the detection of a tap and the time window.
 *
 * @return The wait time from the detection of a tap event to the start of
 *         the time window during which a possible second tap event can be
 *         detected in milliseconds.
 */
int adxl345_getTapLatency (float *latency);
/**
 * Set the tap latency between the detection of a tap and the time window.
 *
 * @param latency_ms The wait time from the detection of a tap event to the
 *                   start of the time window during which a possible
 *                   second tap event can be detected in milliseconds.
 *                   A value of 0 disables the double tap function.
 */
int adxl345_setTapLatency (float latency);
/**
 * Get the time of window between tap latency and a double tap.
 *
 * @return The amount of time after the expiration of the latency time
 *         during which a second valid tap can begin, in milliseconds.
 */
int adxl345_getWindowTime (float *time);
/**
 * Set the time of the window between tap latency and a double tap.
 *
 * @param window_ms The amount of time after the expiration of the latency
 *                  time during which a second valid tap can begin,
 *                  in milliseconds.
 */
int adxl345_setWindowTime (float time);
/**
 * Get the threshold value for detecting activity.
 *
 * @return The threshold value for detecting activity as an 8-bit number.
 *         Scale factor is 62.5mg/LSB.
 */
int adxl345_getActivityThreshold (float *thresh);
/**
 * Set the threshold value for detecting activity.
 *
 * @param threshold The threshold value for detecting activity as an 8-bit
 *                  number. Scale factor is 62.5mg/LSB. A value of 0 may
 *                  result in undesirable behavior if the activity
 *                  interrupt is enabled.
 */
int adxl345_setActivityThreshold (float thresh);
/**
 * Get the threshold value for detecting inactivity.
 *
 * @return The threshold value for detecting inactivity as an 8-bit number.
 *         Scale factor is 62.5mg/LSB.
 */
int adxl345_getInactivityThreshold (float *thresh);
/**
 * Set the threshold value for detecting inactivity.
 *
 * @param threshold The threshold value for detecting inactivity as an
 *                  8-bit number. Scale factor is 62.5mg/LSB.
 */
int adxl345_setInactivityThreshold (float thresh);
/**
 * Get the time required for inactivity to be declared.
 *
 * @return The amount of time that acceleration must be less than the
 *         inactivity threshold for inactivity to be declared, in
 *         seconds.
 */
int adxl345_getTimeInactivity (unsigned char *time);
/**
 * Set the time required for inactivity to be declared.
 *
 * @param inactivity The amount of time that acceleration must be less than
 *                   the inactivity threshold for inactivity to be
 *                   declared, in seconds. A value of 0 results in an
 *                   interrupt when the output data is less than the
 *                   threshold inactivity.
 */
int adxl345_setTimeInactivity (unsigned char time);
/**
 * Get the activity/inactivity control settings.
 *
 *      D7            D6             D5            D4
 * +-----------+--------------+--------------+--------------+
 * | ACT ac/dc | ACT_X enable | ACT_Y enable | ACT_Z enable |
 * +-----------+--------------+--------------+--------------+
 *
 *        D3             D2               D1              D0
 * +-------------+----------------+----------------+----------------+
 * | INACT ac/dc | INACT_X enable | INACT_Y enable | INACT_Z enable |
 * +-------------+----------------+----------------+----------------+
 *
 * See datasheet for details.
 *
 * @return The contents of the ACT_INACT_CTL register.
 */
int adxl345_getActivityInactivityControl (unsigned char *settings);
/**
 * Set the activity/inactivity control settings.
 *
 *      D7            D6             D5            D4
 * +-----------+--------------+--------------+--------------+
 * | ACT ac/dc | ACT_X enable | ACT_Y enable | ACT_Z enable |
 * +-----------+--------------+--------------+--------------+
 *
 *        D3             D2               D1              D0
 * +-------------+----------------+----------------+----------------+
 * | INACT ac/dc | INACT_X enable | INACT_Y enable | INACT_Z enable |
 * +-------------+----------------+----------------+----------------+
 *
 * See datasheet for details.
 *
 * @param settings The control byte to write to the ACT_INACT_CTL register.
 */
int adxl345_setActivityInactivityControl (unsigned char settings);
/**
 * Get the threshold for free fall detection.
 *
 * @return The threshold value for free-fall detection, as an 8-bit number,
 *         with scale factor 62.5mg/LSB.
 */
int adxl345_getFreeFallThreshold (float *thresh);
/**
 * Set the threshold for free fall detection.
 *
 * @return The threshold value for free-fall detection, as an 8-bit number,
 *         with scale factor 62.5mg/LSB. A value of 0 may result in
 *         undesirable behavior if the free-fall interrupt is enabled.
 *         Values between 300 mg and 600 mg (0x05 to 0x09) are recommended.
 */
int adxl345_setFreeFallThreshold (float thresh);
/**
 * Get the time required to generate a free fall interrupt.
 *
 * @return The minimum time that the value of all axes must be less than
 *         the freefall threshold to generate a free-fall interrupt, in
 *         milliseconds.
 */
int adxl345_getFreeFallTime (unsigned char *freefall);
/**
 * Set the time required to generate a free fall interrupt.
 *
 * @return The minimum time that the value of all axes must be less than
 *         the freefall threshold to generate a free-fall interrupt, in
 *         milliseconds. A value of 0 may result in undesirable behavior
 *         if the free-fall interrupt is enabled. Values between 100 ms
 *         and 350 ms (0x14 to 0x46) are recommended.
 */
int adxl345_setFreeFallTime (unsigned char freefall);
/**
 * Get the axis tap settings.
 *
 *      D3           D2            D1             D0
 * +----------+--------------+--------------+--------------+
 * | Suppress | TAP_X enable | TAP_Y enable | TAP_Z enable |
 * +----------+--------------+--------------+--------------+
 *
 * (D7-D4 are 0s).
 *
 * See datasheet for more details.
 *
 * @return The contents of the TAP_AXES register.
 */
int adxl345_getTapAxisControl (unsigned char *settings);
/**
 * Set the axis tap settings.
 *
 *      D3           D2            D1             D0
 * +----------+--------------+--------------+--------------+
 * | Suppress | TAP_X enable | TAP_Y enable | TAP_Z enable |
 * +----------+--------------+--------------+--------------+
 *
 * (D7-D4 are 0s).
 *
 * See datasheet for more details.
 *
 * @param The control byte to write to the TAP_AXES register.
 */
int adxl345_setTapAxisControl (unsigned char settings);
/**
 * Get the source of a tap.
 *
 * @return The contents of the ACT_TAP_STATUS register.
 */
int adxl345_getTapSource (unsigned char *tapSource);
/**
* Set the data rate. (BW_RATE register)
*
* @param rate The rate code (see #defines or datasheet).

 * Set the power mode. (BW_RATE register)
 *
 * @param mode 0 -> Normal operation.
 *             1 -> Reduced power operation.
 */
int adxl345_setDataRateAndPowerModeControl (unsigned char mode);
int adxl345_getDataRateAndPowerModeControl (unsigned char *mode);
/**
 * Set the power control settings.
 *
 * See datasheet for details.
 *
 * @param The control byte to write to the POWER_CTL register.
 */
int adxl345_setPowerSavingControl (unsigned char settings);
  /**
 * Get the power control settings.
 *
 * See datasheet for details.
 *
 * @return The contents of the POWER_CTL register.
 */
int adxl345_getPowerSavingControl (unsigned char *settings);
/**
 * Set the interrupt enable settings.
 *
 * @param settings The control byte to write to the INT_ENABLE register.
 */
int adxl345_setInterruptEnableControl (unsigned char settings);
/**
 * Get the interrupt enable settings.
 *
 * @return The contents of the INT_ENABLE register.
 */
int adxl345_getInterruptEnableControl (unsigned char *settings);
/**
 * Set the interrupt mapping settings.
 *
 * @param settings The control byte to write to the INT_MAP register.
 */
int adxl345_setInterruptMappingControl (unsigned char settings);
/**
 * Get the interrupt mapping settings.
 *
 * @return The contents of the INT_MAP register.
 */
int adxl345_getInterruptMappingControl (unsigned char *settings);
/**
 * Get the interrupt source.
 *
 * @return The contents of the INT_SOURCE register.
 */
int adxl345_getInterruptSource (unsigned char *settings);
/**
 * Set the data format settings.
 *
 * @param settings The control byte to write to the DATA_FORMAT register.
 */
int adxl345_setDataFormatControl (unsigned char settings);
/**
 * Get the data format settings.
 *
 * @return The contents of the DATA_FORMAT register.
 */
int adxl345_getDataFormatControl (unsigned char *settings);
/**
 * Get the output of all three axes.
 *
 * @param Pointer to a buffer to hold the accelerometer value for the
 *        x-axis, y-axis and z-axis [in that order].
 */
int adxl345_getOutput (volatile short *x, volatile short *y, volatile short *z);
/**
 * Get the FIFO control settings.
 *
 * @return The contents of the FIFO_CTL register.
 */
int adxl345_getFifoControl (unsigned char *settings);
/**
 * Set the FIFO control settings.
 *
 * @param The control byte to write to the FIFO_CTL register.
 */
int adxl345_setFifoControl (unsigned char settings);
/**
 * Get FIFO status.
 *
 * @return The contents of the FIFO_STATUS register.
 */
int adxl345_getFifoStatus (unsigned char *settings);

#endif /* ADXL345_H_ */
